'use strict';

var path = require('path');
var webpack = require('webpack');
var ExtractTextPlugin = require('extract-text-webpack-plugin');

module.exports = {
  devtool: 'source-map',
  context: path.join(__dirname, 'public'),
  entry: {
    vendor: ['react'],
    app: './index.jsx'
  },
  
  output: {
    path: path.join(__dirname, 'dist'),
    filename: '[name].min.js',
    publicPath: '/dist/'
  },

  resolve: {
    extensions: ['', '.js', '.jsx']
  },

  module: {
    loaders: [
      {test: /\.hjs$/, loader: 'file?name=[name].html'},
      {test: /\.s?css$/, loader: ExtractTextPlugin.extract('style', 'css?sourceMap!autoprefixer!sass?sourceMap')},
      {test: /\.jsx?$/, loader: 'jsx?harmony'}
    ]
  },

  plugins: [
    new webpack.DefinePlugin({
      'progress.env': {
        'NODE_ENV': JSON.stringify('production')
      }
    }),
    new webpack.optimize.DedupePlugin(),
    new webpack.optimize.UglifyJsPlugin({
      compress: {warnings: false}
    }),
    new webpack.optimize.CommonsChunkPlugin({
      name: 'vendor',
      filename: '[name].min.js',
      minChunks: Infinity
    }),
    new ExtractTextPlugin('[name].min.css', {
      // set the option `allChunks` to `true`
      // `chunkFilename` will don’t contain embedded styles
      allChunks: true
    })
  ]
};
