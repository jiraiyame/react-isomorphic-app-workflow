# react-isomorphic-app-workflow
---
A tiny react isomorphic app workflow

### Installation

```
npm install
```

### Running

Development

```
$ npm start
```

Production

```
$ npm run build
```

### Using

- Development: `http://localhost:8080/dist`
- Production: `http://localhost:8080`


### License

MIT