'use strict';

var path = require('path');
var util = require('util');
var webpack = require('webpack');
var pkg = require('./package.json');

var devHost = pkg.config.devHost;
var devPort = pkg.config.devPort;

var htmlLoader = [
  'file?name=[name].html',
  'template-html?' + [
    'raw=true',
    'engine=hogan',
    'development=true',
    'publicPath=http://localhost:8080'
  ].join('&')
].join('!');

module.exports = {
  devtool: 'inline-source-map',
  context: path.join(__dirname, 'public'),
  entry: {
    app: [
      util.format('webpack-dev-server/client?http://%s:%d/dist/', devHost, devPort),
      'webpack/hot/only-dev-server',
      './index.jsx'
    ]
  },

  output: {
    filename: '[name].js',
    path: path.join(__dirname, 'dist'),
    publicPath: util.format('http://%s:%d/dist', devHost, devPort)
  },

  resolve: {
    extensions: ['', '.js', '.jsx']
  },

  module: {
    loaders: [
      {test: /\.hjs$/, loader: htmlLoader},
      {test: /\.s?css$/, loader: 'style!css?sourceMap!sass?sourceMap'},
      {test: /\.jsx?$/, loader: 'react-hot!jsx?harmony'}
    ]
  },

  plugins: [
    new webpack.HotModuleReplacementPlugin(),
    new webpack.NoErrorsPlugin(),
    new webpack.DefinePlugin({
      'process.env': {
        'NODE_ENV': JSON.stringify('development')
      }
    })
  ]
};
