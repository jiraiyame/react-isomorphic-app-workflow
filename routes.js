
var express = require('express');
var router = express.Router();

require('node-jsx').install({
  harmony: true,
  extension: '.jsx'
});

var React = require('react');
var Counter = require('./public/components/Counter');
var CounterFactory = React.createFactory(Counter);

router.get('/', function(req, res) {
  var state = Counter.getDataFromServer();
  var markup = React.renderToString(
    CounterFactory({counter: state})
  );

  res.render('index', {
    production: true,
    markup: markup,
    initialState: state
  });
});

module.exports = router;
