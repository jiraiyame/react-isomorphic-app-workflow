
var React = require('react');

var Counter = React.createClass({
  statics: {
    getDataFromServer: function() {
      return 2;
    }
  },

  getInitialState: function() {
    return {
      counter: this.props.counter || 0
    }
  },

  increment: function(counter) {
    this.setState({counter: counter + 1});
  },

  decrement: function(counter) {
    this.setState({counter: counter - 1});
  },

  render: function() {
    var counter = this.state.counter;
    
    return (
      <div className="counter-app">
        <p>Added: 
          <strong>{counter}</strong>
          {counter === 1 ? 'item' : 'items'}
        </p>
        <button onClick={this.increment.bind(null, counter)}>+</button>
        <button onClick={this.decrement.bind(null, counter)} disabled={counter === 0}>-</button>
      </div>
    );
  }
});

module.exports = Counter;
