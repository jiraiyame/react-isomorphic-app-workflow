
var React = require('react');
var Counter = require('./components/Counter');

require('normalize.css');
require('./styles/app.scss');

process.env.NODE_ENV === 'development'
  ? require('../views/index.hjs')
  : undefined;

var state = window._initialState;

React.render(
  <Counter counter={state} />,
  document.getElementById('app')
);
