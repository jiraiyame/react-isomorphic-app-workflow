
var path = require('path');
var React = require('react');
var express = require('express');
var bodyParser = require('body-parser');
var cookieParser = require('cookie-parser');

var index = require('./routes');
var pkg = require('./package.json');

var app = express();

// Middleware
app.use(bodyParser.json({limit: '1mb'}));
app.use(bodyParser.urlencoded({extended: true, limit: '1mb'}));
app.use(cookieParser());

// View setting
app.set('views', path.join(__dirname, 'views'));
app.set('view engine', 'hjs');

// Statics
app.use(express.static(path.join(__dirname)));

// Routes
app.use('/', index);

// Development error handler
if (app.get('env') === 'development') {
  app.use(function(err, req, res, next) {
    res.status(err.status || 500);
    res.render('error', {
      message: err.message,
      error: err
    });
  });
}

// Production error handler
app.use(function(err, req, res, next) {
  res.status(err.status || 500);
  res.render('error', {
    message: err.message,
    error: {}
  });
});

// Starting...
app.listen(pkg.config.devPort);

console.log('Listening on port: %d', pkg.config.devPort);

module.exports = app;
